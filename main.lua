
os.loadAPI(shell.resolve("Surface"))

local width, height = term.getSize()
local surface = Surface.create(width, height, " ", colors.black, colors.white)
local timerSpeed = 0.09
local timer = os.startTimer(timerSpeed)
local running = true

local start = true
local startTimer = 60
local pacmanX = 8
local pacmanY = 1
local pacmanAni = 0

local pacmanDir = 0
local scroll = 0
local pacmanFinalDir = 0
local pacmanFrame = 0
local worldOffset = 0
local score = 0
--0 = pellet, 1 = wall, 2 = powerpellet, 3 = blank
local map = {
	{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};
	{1,2,0,0,0,0,0,0,0,0,0,0,0,0,0,2,1};
	{1,0,1,1,0,1,1,0,1,0,1,1,0,1,1,0,1};
	{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1};
	{1,0,1,1,0,1,1,0,1,0,1,1,0,1,1,0,1};
	{1,0,1,1,0,1,0,0,1,0,0,1,0,1,1,0,1};
	{1,0,0,0,0,1,0,1,1,1,0,1,0,0,0,0,1};
	{1,1,0,1,0,0,0,0,0,0,0,0,0,1,0,1,1};
	{1,1,0,1,0,1,1,1,1,1,1,1,0,1,0,1,1};
	{0,0,0,0,0,1,3,3,3,3,3,1,0,0,0,0,0};
	{1,1,0,1,0,1,1,1,1,1,1,1,0,1,0,1,1};
	{1,1,0,1,0,0,0,0,0,0,0,0,0,1,0,1,1};
	{1,2,0,0,0,1,1,0,1,0,1,1,0,0,0,2,1};
	{1,0,1,1,0,0,0,0,0,0,0,0,0,1,1,0,1};
	{1,0,0,1,0,1,0,1,1,1,0,1,0,1,0,0,1};
	{1,1,0,0,0,1,0,0,1,0,0,1,0,0,0,1,1};
	{1,1,0,1,1,1,1,0,1,0,1,1,1,1,0,1,1};
	{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1};
	{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};
}
function drawMap(surf)
	for y = 1, #map do
		for x = 1, #map[y] do
			local drawX = ((x-1)*3)+1
			local drawY = (((y-1)*3)+1)-scroll+1
			
			if map[y][x] == 1 then
				local left, right, up, down = false, false, false, false
				if x > 1 then
					left = (map[y][x-1] == 1)
				end
				if y > 1 then
					up = (map[y-1][x] == 1)
				end
				if x < #map[1] then 
					right = (map[y][x+1] == 1)
				end
				if y < #map then
					down = (map[y+1][x] == 1)
				end
				local chr = "+-+\n| |\n+-+"
				
				if not left and not right and not up and not down then
					chr = "+-+\n| |\n+-+"
				end
				if left and not right and not up and not down then
					chr = "--+\n  |\n--+"
				end
				if not left and right and not up and not down then
					chr = "+--\n|  \n+--"
				end
				if left and right and not up and not down then
					chr = "---\n   \n---"
				end
				if right and down and not left and not up then
					chr = "+--\n|  \n| +"
				end
				if left and down and not right and not up then
					chr = "--+\n  |\n+ |"
				end
				if right and up and not down and not left then
					chr = "| +\n|  \n+--"
				end
				if left and up and not down and not right then
					chr = "+ |\n  |\n--+"
				end
				if up and down and not left and not right then
					chr = "| |\n| |\n| |"
				end
				if down and right and left and not up then
					chr = "---\n   \n+ +"
				end
				if up and not right and not left and not down then
					chr = "| |\n| |\n+-+"
				end
				if down and not up and not right and not left then
					chr = "+-+\n| |\n| |"
				end
				if right and down and not left and up then
					chr = "| +\n|  \n| +"
				end
				if up and not down and left and not right then
					chr = "+ |\n  |\n--+"
				end
				if left and down and not right and up then
					chr = "+ |\n  |\n+ |"
				end
				if left and up and right and not down then
					chr = "+ +\n   \n---"
				end
				surf:drawText(drawX, drawY, chr, colors.blue, colors.black)
			elseif map[y][x] == 0 then
				surf:drawText(drawX, drawY, "   \n " ..string.char(7).. " \n   ", nil, colors.yellow)
			elseif map[y][x] == 2 then
				surf:drawText(drawX, drawY, "   \n O \n   ", nil, colors.yellow)
			elseif map[y][x] == 3 then
				surf:drawText(drawX, drawY, "   \n   \n   ", nil, colors.white)
			end
		end
	end
end

function clamp(val, min, max)
	return math.min(math.max(val, min), max)
end
local ppellet = false
local ppellet_timer = 240
local ghosts = {}
local ghostIndex = {
	["red"]	 	= {
		x = 0;
		y = 0;
		ani = 0;
		init = function(self)
		
		end;
		update = function(self)
		
		end;
	};
	["green"]	= {};
	["blue"] 	= {};
	["pink"] 	= {};
}
function addGhost(type)
	ghosts[type] = ghostIndex[type]
end
local allPellets = 0
for i = 1, #map do
	for w = 1, #map[i] do
		if map[i][w] == 0 or map[i][w] == 2 then
			allPellets = allPellets + 1
		end
	end
end
function win()
	surface:clear(" ", colors.black, colors.white)
	surface:drawText(math.floor(width/2)-math.floor(string.len("Congratulations!")/2), 2, "Congratulations!", nil, colors.red)
	surface:drawText(math.floor(width/2)-math.floor(string.len("You beat the game!")/2), 4, "You beat the game!", nil, colors.white)
	running = false
end
while running do
	local e = {os.pullEvent()}
	
	if e[1] == "timer" then
		if e[2] == timer then
			surface:clear(" ", colors.black, colors.white)
			if not start then 
				if map[pacmanY+1][pacmanX+1] == 0 then
					map[pacmanY+1][pacmanX+1] = 3
					score = score + 20
					allPellets = allPellets - 1
				end
				if map[pacmanY+1][pacmanX+1] == 2 then
					map[pacmanY+1][pacmanX+1] = 3
					score = score + 100
					ppellet = true
				end
			end
			if start then
				if startTimer > 0 then startTimer = startTimer - 5 else start = false end
			end
			drawMap(surface)
			if not start then 
				pacmanFrame = pacmanFrame + 1
				if pacmanFrame > 1 then pacmanFrame = 0 end
				if pacmanAni == 0 then 
					if pacmanFinalDir == 0 then
						if map[pacmanY+1][pacmanX+2] ~= 1 and map[pacmanY+1][pacmanX+2] ~= 4 then
							pacmanDir = pacmanFinalDir 
						end
					end
					if pacmanFinalDir == 1 then
						if map[pacmanY+1][pacmanX] ~= 1 and map[pacmanY+1][pacmanX] ~= 4 then
							pacmanDir = pacmanFinalDir 
						end
					end
					if pacmanFinalDir == 2 then
						if map[pacmanY+2][pacmanX+1] ~= 1 and map[pacmanY+2][pacmanX+1] ~= 4 then
							pacmanDir = pacmanFinalDir 
						end
					end
					if pacmanFinalDir == 3 then
						if map[pacmanY][pacmanX+1] ~= 1 and map[pacmanY][pacmanX+1] ~= 4 then
							pacmanDir = pacmanFinalDir 
						end
					end
					
				end
				if ppellet then
					if ppellet_timer > 0 then ppellet_timer = ppellet_timer - 3; end
					if ppellet_timer <= 0 then ppellet_timer = 240; ppellet = false; end
				end
				pacmanAni = pacmanAni + 1
				
				if pacmanDir == 0 and map[pacmanY+1][pacmanX+2] == 1 or map[pacmanY+1][pacmanX+2] == 4 then pacmanAni = 0 end
				if pacmanDir == 1 and map[pacmanY+1][pacmanX] == 1 or map[pacmanY+1][pacmanX] == 4 then pacmanAni = 0 end
				if pacmanDir == 2 and map[pacmanY+2][pacmanX+1] == 1 or map[pacmanY+2][pacmanX+1] == 4 then pacmanAni = 0 end
				if pacmanDir == 3 and map[pacmanY][pacmanX+1] == 1 or map[pacmanY][pacmanX+1] == 4 then pacmanAni = 0 end
				
				if pacmanAni >= 3 then
					if pacmanDir == 0 then 
						if map[pacmanY+1][pacmanX+2] ~= 1 and map[pacmanY+1][pacmanX+2] ~= 4 then
							pacmanX = pacmanX + 1; 
						end
					end
					if pacmanDir == 1 then 
						if map[pacmanY+1][pacmanX] ~= 1 and map[pacmanY+1][pacmanX] ~= 4 then
							pacmanX = pacmanX - 1; 
						end
					end
					if pacmanDir == 2 then 
						if map[pacmanY+2][pacmanX+1] ~= 1 and map[pacmanY+2][pacmanX+1] ~= 4 then
							pacmanY = pacmanY + 1; 
						end
					end
					if pacmanDir == 3 then 
						if map[pacmanY][pacmanX+1] ~= 1 and map[pacmanY][pacmanX+1] ~= 4 then
							pacmanY = pacmanY - 1;  
						end
					end
					pacmanAni = 0; 
				end
				if pacmanDir == 0 then
					if pacmanX > #map[1] then
						pacmanX = -1
					end
				end
				if pacmanDir == 1 then
					if pacmanX < 0 then
						pacmanX = math.floor(width/3)+1
					end
				end
				if pacmanDir == 2 then 
					local pacman = (((pacmanY + 1) * 3) + pacmanAni) - scroll
					local center = math.floor(height / 2)
					
					if pacman > center then						
						scroll = scroll + 1
					end
				end
				if pacmanDir == 3 then
					local pacman = (((pacmanY + 1) * 3) - pacmanAni) - scroll
					local center = math.floor(height / 2)
						
					if pacman < center then						
						scroll = scroll - 1
					end
				end
				scroll = clamp(scroll, 0, #map+height+1)
				local pacy = pacmanY * 3
				local pdrawX = (pacmanX * 3) + 1
				local pdrawY = (pacmanY * 3) + 2
				
				if pacmanDir == 0 then 
					if pacmanFrame == 0 then
						surface:drawText(pdrawX+pacmanAni, pdrawY-scroll, "/" .. string.char(175) .. ">\n|< \n\\_>", nil, colors.yellow)
					else
						surface:drawText(pdrawX+pacmanAni, pdrawY-scroll, "/"..string.char(175).."\\\n|-+\n\\_/", nil, colors.yellow)
					end
				elseif pacmanDir == 1 then
					if pacmanFrame == 0 then
						surface:drawText(pdrawX-pacmanAni, pdrawY-scroll, "<" ..string.char(175).. "\\\n >|\n<_/", nil, colors.yellow)
					else
						surface:drawText(pdrawX-pacmanAni, pdrawY-scroll, "/"..string.char(175).."\\\n+-|\n\\_/", nil, colors.yellow)
					end
				elseif pacmanDir == 2 then

					local pacy = pacmanY * 3
		
					if pacmanFrame == 0 then
						surface:drawText(pdrawX, pdrawY+pacmanAni-scroll, "/"..string.char(175).."\\\n|A|\nV V", nil, colors.yellow)
					else
						surface:drawText(pdrawX, pdrawY+pacmanAni-scroll, "/"..string.char(175).."\\\n|||\n\\+/", nil, colors.yellow)
					end
				elseif pacmanDir == 3 then
					local pacy = pacmanY * 3
					
					if pacmanFrame == 0 then
						surface:drawText(pdrawX, pdrawY-pacmanAni-scroll, "A A\n|V|\n\\_/", nil, colors.yellow)
					else
						surface:drawText(pdrawX, pdrawY-pacmanAni-scroll, "/T\\\n|||\n\\_/", nil, colors.yellow)
					end
				end
			end
			surface:drawText(1, 1, string.rep(" ", width), colors.black, colors.white)
			surface:drawText(2, 1, "SCORE: ", nil, colors.lightBlue)
			surface:drawText(9, 1, string.format("%08d", score), nil, colors.white)
			
			if ppellet then surface:drawText(math.floor(width/2)-math.floor(string.len("!!!")/2), 1, "!!!", nil, colors.red) end
			if start then surface:drawText(math.floor(width/2)-math.floor(string.len("START!")/2), 1, "START!", nil, colors.lime) end
			if score >= 3480 then
				win()
			end
			surface:render(term)
			timer = os.startTimer(timerSpeed)
		end
	elseif e[1] == "key" then
		if e[2] == keys.right then
			pacmanFinalDir = 0
		end
		if e[2] == keys.down then
			pacmanFinalDir = 2
		end
		if e[2] == keys.left then
			pacmanFinalDir = 1
		end
		if e[2] == keys.up then
			pacmanFinalDir = 3
		end
		if e[2] == keys.q then
			running = false
		end
	end
end

os.unloadAPI(shell.resolve("Surface"))
